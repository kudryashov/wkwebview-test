//
//  ViewController.swift
//  WebViewTest
//
//  Created by Anton Kudryashov on 24.03.2021.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {
	let webview = WKWebView()

	override func viewDidLoad() {
		super.viewDidLoad()
		view.addSubview(webview)

		view.backgroundColor = .gray
		webview.backgroundColor = .magenta
		webview.navigationDelegate = self

		loadWebview()
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		webview.frame = view.bounds
	}

	private func loadWebview() {
		//webview.loadHTMLString(Env.html, baseURL: nil)
		webview.load(.init(url: Env.url))
	}

	func webView(
		_ webView: WKWebView,
		decidePolicyFor navigationAction: WKNavigationAction,
		decisionHandler: @escaping (WKNavigationActionPolicy) -> Void
	) {
		print("handle \(navigationAction.request.url?.absoluteString ?? "none")")
		decisionHandler(.allow)
	}
}

fileprivate enum Env {
	static let url = URL(string: "https://cdn-app.sberdevices.ru/misc/0.0.0/catalog-launcher/k.html")!
	static let html: String = """
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	  "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	 <head>
	   <title>!DOCTYPE</title>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	 </head>
	 <body>
	  <p>Разум — это Будда, а прекращение умозрительного мышления — это путь.
	  Перестав мыслить понятиями и размышлять о путях существования и небытия,
	  о душе и плоти, о пассивном и активном и о других подобных вещах,
	  начинаешь осознавать, что разум — это Будда,
	  что Будда — это сущность разума,
	  и что разум подобен бесконечности.</p>
	 </body>
	</html>
	"""
}
